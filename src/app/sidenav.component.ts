import { Component, ViewEncapsulation, OnInit, ChangeDetectorRef  } from '@angular/core';
import * as d3 from "d3";

@Component({
  selector: 'my-sidebar',  
  templateUrl: './src/app/templates/sidenav.html',
  styleUrls: ['./src/app/templates/sidenav.css'],
  encapsulation: ViewEncapsulation.None
})

export class SidenavComponent {

  rerender = false;
  constructor(private cdRef:ChangeDetectorRef){
    this.resize(true);
  }
  sidenavClass = 'glyphicon glyphicon-chevron-left';

  activeDaily = 'active';
  activeWeekly = '';
  activeMontly = '';

  public isCollapsed = true;  

  options = {
    chart: {
        type: 'lineChart',
        height: 450,
        margin : {
            top: 20,
            right: 20,
            bottom: 40,
            left: 55
        },
        x: function(d){ return d.x; },
        y: function(d){ return d.y; },
        useInteractiveGuideline: true,
        dispatch: {
            stateChange: function(e){ console.log("stateChange"); },
            changeState: function(e){ console.log("changeState"); },
            tooltipShow: function(e){ console.log("tooltipShow"); },
            tooltipHide: function(e){ console.log("tooltipHide"); }
        },
        xAxis: {
            axisLabel: 'Daily'
        },
        yAxis: {            
            axisLabel: 'Records',
            tickFormat: function(d){
                return d3.format('.02f')(d);
            },
            axisLabelDistance: -10
        },
        callback: function(chart){
            console.log("!!! lineChart callback !!!");
        }
    },
    title: {
        enable: true,
        text: 'Daily Line Chart'
    },
    subtitle: {
        enable: true,        
        text: '',
        css: {
            'text-align': 'center',
            'margin': '10px 13px 0px 7px'
        }
    },
    caption: {
        enable: true,        
        html: '<b>Figure 1.</b> Daily Line Chart',
        css: {
            'text-align': 'justify',
            'margin': '10px 13px 0px 7px'
        }
    }
  };

  data = this.dailyData();

  clickSidenav(sidenav, event){
    sidenav.toggle();    

    if(this.sidenavClass == 'glyphicon glyphicon-chevron-left'){
      this.sidenavClass = 'glyphicon glyphicon-chevron-right';
    }else{
      this.sidenavClass = 'glyphicon glyphicon-chevron-left';
    }    
  }    

  /*Random daily Data Generator */
  dailyData() {
      var spo2Values = [],sleepHourValues = [],
          pulseValues = [], faceValues = [];

      //Data is represented as an array of {x,y} pairs.
      for (var i = 0; i < 100; i++) {
          spo2Values.push({x: i, y: Math.sin(i/10)});
          sleepHourValues.push({x: i, y: i % 10 == 5 ? null : Math.sin(i/10) *0.25 + 0.5});
          pulseValues.push({x: i, y: .5 * Math.cos(i/10+ 2) + Math.random() / 10});
          faceValues.push({x: i, y: .5 * Math.cos(i/10)});
      }      

      //Line chart data should be sent as an array of series objects.
      return [
          {
              values: spo2Values,      //values - represents the array of {x,y} data points
              key: 'SpO2', //key  - the name of the series.
              color: '#ff7f0e',  //color - optional: choose your own line color.
              strokeWidth: 2,
              classed: 'dashed'
          },
          {
              values: sleepHourValues,
              key: 'Sleep Hours',
              color: '#2ca02c'
          },
          {
              values: pulseValues,
              key: 'Pulse',
              color: '#7777ff',
              //area: true      //area - set to true if you want this line to turn into a filled area chart.
          },
          {
              values: faceValues,
              key: 'Face Orientation',
              color: 'yellow',                    
          }
      ];



  };

  /*Random weekly Data Generator */
  weeklyData() {
      var spo2Values = [],sleepHourValues = [],
          pulseValues = [], faceValues = [];

      //Data is represented as an array of {x,y} pairs.
      for (var i = 0; i < 100; i++) {
          spo2Values.push({x: i, y: Math.sin(i*0.75/10)});
          sleepHourValues.push({x: i, y: i % 10 == 2 ? null : Math.sin(i/10) *0.25 + 0.75});
          pulseValues.push({x: i, y: .5 * Math.cos(i/10+ 2) + Math.random()*0.02 / 10});
          faceValues.push({x: i, y: .5 * Math.cos(i/10+4)});
      }

      //Line chart data should be sent as an array of series objects.
      return [
          {
              values: spo2Values,      //values - represents the array of {x,y} data points
              key: 'SpO2', //key  - the name of the series.
              color: '#ff7f0e',  //color - optional: choose your own line color.
              strokeWidth: 2,
              classed: 'dashed'
          },
          {
              values: sleepHourValues,
              key: 'Sleep Hours',
              color: '#2ca02c'
          },
          {
              values: pulseValues,
              key: 'Pulse',
              color: '#7777ff',
              //area: true      //area - set to true if you want this line to turn into a filled area chart.
          },
          {
              values: faceValues,
              key: 'Face Orientation',
              color: 'yellow',                    
          }
      ];
  };	

  /*Random monthly Data Generator */
  monthlyData() {
      var spo2Values = [],sleepHourValues = [],
          pulseValues = [], faceValues = [];

      //Data is represented as an array of {x,y} pairs.
      for (var i = 0; i < 100; i++) {
          spo2Values.push({x: i, y: Math.cos(i*1.5/10)});
          sleepHourValues.push({x: i, y: i % 10 == 5 ? null : Math.cos(i*0.7/10) *0.75 + 0.25});
          pulseValues.push({x: i, y: .5 * Math.sin(i/10+ 1) + Math.random()*0.4 / 10});
          faceValues.push({x: i, y: .58 * Math.sin(i*0.4/10)});
      }

      //Line chart data should be sent as an array of series objects.
      return [
          {
              values: spo2Values,      //values - represents the array of {x,y} data points
              key: 'SpO2', //key  - the name of the series.
              color: '#ff7f0e',  //color - optional: choose your own line color.
              strokeWidth: 2,
              classed: 'dashed'
          },
          {
              values: sleepHourValues,
              key: 'Sleep Hours',
              color: '#2ca02c'
          },
          {
              values: pulseValues,
              key: 'Pulse',
              color: '#7777ff',
              //area: true      //area - set to true if you want this line to turn into a filled area chart.
          },
          {
              values: faceValues,
              key: 'Face Orientation',
              color: 'yellow',                    
          }
      ];
  };	



  dreamOption(dreamOption){

    this.data[0]['disabled'] = true;
    this.data[1]['disabled'] = true;
    this.data[2]['disabled'] = true;
    this.data[3]['disabled'] = true;
    
    switch (dreamOption) {
      case "spo2":
        this.data[0]['disabled'] = false;
        break;
      case "sleepHour":
        this.data[1]['disabled'] = false;
        break;
      case "pulse":
        this.data[2]['disabled'] = false;
        break;			
      case "sleepPosition":
        this.data[3]['disabled'] = false;
        break;			
      default:
        this.data[0]['disabled'] = false;
    }	

    this.doRender();
  }  


  timeOption(timeOption){
	  
    this.activeDaily = '';
    this.activeWeekly = '';
    this.activeMontly = '';
      
    switch (timeOption) {
      case "daily":
        this.data = this.dailyData();
        this.activeDaily = 'active';
        break;
      case "weekly":
        this.data = this.weeklyData();
        this.activeWeekly = 'active';
        break;
      case "monthly":
        this.data = this.monthlyData();
        this.activeMontly = 'active';
        break;			
      default:
        this.data = this.dailyData();			
    }
	
  }     

  doRender(){
    this.rerender = true;
    this.cdRef.detectChanges();
    this.rerender = false;  
  }

  resize(needResize){

    // adjust bootstrap styles
    setTimeout(function(){
      if(needResize){
        window.dispatchEvent(new Event('resize'));
      }
    }, 1000);  

    
    // validate window width
    var self = this;
    window.addEventListener("resize", function(){

      var w = window.innerWidth;      
      if(w > 992){
        self.isCollapsed = true;
      }

    });    

  }
}