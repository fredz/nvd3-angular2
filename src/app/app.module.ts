//import { NgModule }      from '@angular/core';
import { Component, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AppComponent }  from './app.component';
import { SidenavComponent }  from './sidenav.component';
import { MaterialModule } from '@angular/material';


//import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JsonpModule } from '@angular/http';
import { NvD3Module } from 'angular2-nvd3';

@NgModule({
  imports: [ 
    BrowserModule, 
    NgbModule.forRoot(), 
    FormsModule, 
    ReactiveFormsModule, 
    JsonpModule, 
    MaterialModule,
    NvD3Module 
  ],
  declarations: [ 
    AppComponent,
    SidenavComponent 
  ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }


