var config = {
   entry: './src/main.ts',
	
   output: {
      path:'./',
      filename: 'bundle.js',
   },	
	
   module: {
      loaders: [
         {
            test: /\.ts$/,            
            loader: 'ts-loader',
            exclude: /node_modules/
         }
      ]
   },

   resolve:{
       extensions: ['*','.js', '.ts']
   }
}

module.exports = config;